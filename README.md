# README

## Manual MarkDown

---

### Nivel

Este es un manual sobre sintaxis MarkDown, al contrario que los editores como 
LibreOffice que son WYSIWYG.

Esto es una **negrita**.

Esto es _cursiva_.

**Listas**

*Elemento
*Elemento
*Elemento

Lista ordenada:

1. Uno
2. Dos
3. Tres

Código multilinea:


```php
<?php

echo "Hola mundo!";

?>
```

Código en una sola línea es así `echo "Hola mundo";`.

Un enlace se hace así [Texto del enlace] (https://gitlab.com/ikerrr87532/php.git)

Incrustar una imagen:

![texto alt](img/01.png)

>Esto es una cita de Marx